from functools import partial
import django
from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import (Employee, )
from .serializers import (EmployeeSerializer, EmployeeSerializerPOST)
from rest_framework import status
from rest_framework.views import APIView
from django.http import Http404
# Create your views here.


class EmployeeView(APIView):
    def get_object(self, pk):
        try:
            return Employee.objects.get(pk = pk)
        except Employee.DoesNotExist:
            raise Http404

    def get(self, request):
        emp_id = request.query_params.get("id", None)
        if emp_id is None:
            emp_data_all = Employee.objects.all()
            serialized_emp_data_all = EmployeeSerializer(emp_data_all, many = True)
            return Response(serialized_emp_data_all.data)
        
        return Response(EmployeeSerializer(self.get_object(emp_id)).data)

    def post(self, request):
        request_data = request.data
        serialized_request_data = EmployeeSerializer(data=request_data)
        if serialized_request_data.is_valid():
            serialized_request_data.save()
            return Response({"status": "ok"})
        return Response(serialized_request_data.errors, status.HTTP_400_BAD_REQUEST)

    def put(self, request):
        patched_data = request.data
        emp_id = patched_data.get("emp_id", None)
        emp_data = self.get_object(emp_id)
        serialized_data = EmployeeSerializer(emp_data, data = patched_data)
        if serialized_data.is_valid():
            serialized_data.save()
            return Response({"status": "ok"})
        return Response(serialized_data.errors, status.HTTP_400_BAD_REQUEST)

    def patch(self, request):
        patched_data = request.data
        emp_id = patched_data.get("emp_id", None)
        emp_data = self.get_object(emp_id)
        serialized_data = EmployeeSerializer(emp_data, data = patched_data, partial = True)
        if serialized_data.is_valid():
            serialized_data.save()
            return Response({"status": "ok"})
        return Response(serialized_data.errors, status.HTTP_400_BAD_REQUEST)

    def delete(self, request):
        emp_id = request.query_params.get("id", None)
        emp_data = self.get_object(emp_id)
        emp_data.delete()
        return Response({"status": "ok"})
