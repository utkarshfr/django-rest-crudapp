from django.db import models

# Create your models here.
class Department(models.Model):
    dept_id = models.BigAutoField(primary_key = True)
    dept_name = models.CharField(max_length = 20, null = False, blank = False, unique = True)

    def __str__(self) -> str:
        return f"{self.dept_id} | {self.dept_name}"


class Employee(models.Model):
    emp_id = models.BigAutoField(primary_key = True)
    emp_name = models.CharField(max_length = 100, null = False, blank = False)
    address = models.CharField(max_length = 100, null = False, blank = False)
    salary = models.PositiveIntegerField(null = False, blank = False)
    dept = models.ForeignKey("Department", related_name = "employees", on_delete = models.CASCADE)

    def __str__(self) -> str:
        return f"{self.emp_id} | {self.emp_name} | {self.address} | {self.salary}"
