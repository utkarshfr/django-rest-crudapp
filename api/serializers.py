from rest_framework import serializers
from .models import (Employee, Department)


class DepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Department
        fields = "__all__"


class EmployeeSerializerPOST(serializers.ModelSerializer):
    class Meta:
        model = Employee
        fields = ['emp_id', 'emp_name', 'address', 'salary', 'dept']


class EmployeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Employee
        fields = "__all__"

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['dept'] = DepartmentSerializer(instance.dept).data
        return response